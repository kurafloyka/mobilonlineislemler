Specification Heading
=====================
Created by mac on 31.12.2020

This is an executable specification file which follows markdown syntax.
Every heading in this file denotes a scenario. Every bulleted point denotes a step.
     
Uygulama Acilmasi
---------------------------------------------
Tags:UygulamaAcilmasi
* "broadbandBar" li elementi bul ve tıkla
* "broadbandTCKN" li elementi bul, temizle ve "43507670642" değerini yaz
* "broadbandGSM" li elementi bul, temizle ve "5012223325" değerini yaz
* "broadbandOtpButton" li elementi bul ve tıkla
* Wait "10" seconds
* OTP Sifre Bilgisi Girilir
* "broadbandOtpLoginButon" li elementi bul ve tıkla
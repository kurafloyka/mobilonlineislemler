import com.thoughtworks.gauge.Step;
import element.ReadFiles;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.support.ui.ExpectedConditions;


import static java.time.Duration.ofMillis;

public class StepImplementation extends HookImplementation {
    @Step("Uygulamayi ac")
    public void openApp() {

        //driver.findElement(ReadFiles.readLocator("siparislerim")).click();

        wait.until(ExpectedConditions.visibilityOfElementLocated
                (ReadFiles.readLocator("gsm"))).sendKeys("5012223471");
        log.info("GSM NO girildi...");

        wait.until(ExpectedConditions.visibilityOfElementLocated
                (ReadFiles.readLocator("password"))).sendKeys("123456");
        log.info("Sifre girildi...");

        wait.until(ExpectedConditions.visibilityOfElementLocated
                (ReadFiles.readLocator("btnLogin"))).click();

        log.info("Giris butonuna basildi...");
    }

    @Step({"<key> li elementi bul ve tıkla", "Click element by <key>"})
    public void clickByKey(String key) {


        clickElement(key);


    }

    @Step({"<key> li elementi bul, temizle ve <text> değerini yaz",
            "Find element by <key> clear and send keys <text>"})
    public void sendKeysByKey(String key, String text) {

        clearElement(key);
        sendKeys(key, text);

    }


    @Step({"<key> li elementi bul, temizle",
            "Find element by <key> clear"})
    public void sendKeysByKey(String key) {

        clearElement(key);


    }

    @Step({"<key> li elementi bul ve varsa tıkla", "Click element by <key> if exist"})
    public void existClickByKey(String key) {
    }

    @Step({"<key> li elementi bul ve varsa dokun", "Click element by <key> if exist"})
    public void existTapByKey(String key) {
    }

    @Step({"sayfadaki <X> <Y>  alana dokun"})
    public void coordinateTap(int X, int Y) {
    }


    @Step({"<key> li elementi bul ve <text> değerini yaz",
            "Find element by <key> and send keys <text>"})
    public void sendKeysByKeyNotClear(String key, String text) {
        sendKeys(key, text);
    }

    @Step("<key> li elementi bul ve <text> değerini tek tek yaz")
    public void sendKeysValueOfClear(String key, String text) {

    }


    @Step({"<key> li elementi bul ve değerini <saveKey> olarak sakla",
            "Find element by <key> and save text <saveKey>"})
    public void saveTextByKey(String key, String saveKey) {

    }

    @Step({"<key> li elementi bul ve değerini <saveKey> saklanan değer ile karşılaştır",
            "Find element by <key> and compare saved key <saveKey>"})
    public void equalsSaveTextByKey(String key, String saveKey) {

    }


    @Step({"<key> li ve değeri <text> e eşit olan elementli bul ve tıkla",
            "Find element by <key> text equals <text> and click"})
    public void clickByIdWithContains(String key, String text) {

    }

    @Step(" <yön> yönüne swipe et")
    public void swipe(String yon) {

        Dimension d = driver.manage().window().getSize();
        int height = d.height;
        int width = d.width;

        if (yon.equals("SAĞ")) {

            int swipeStartWidth = (width * 80) / 100;
            int swipeEndWidth = (width * 30) / 100;

            int swipeStartHeight = height / 2;
            int swipeEndHeight = height / 2;

            //appiumDriver.swipe(swipeStartWidth, swipeStartHeight, swipeEndWidth, swipeEndHeight, 1000);
            new TouchAction(driver)
                    .press(PointOption.point(swipeStartWidth, swipeStartHeight))
                    .waitAction(WaitOptions.waitOptions(ofMillis(1000)))
                    .moveTo(PointOption.point(swipeEndWidth, swipeEndHeight))
                    .release()
                    .perform();
        } else if (yon.equals("SOL")) {

            int swipeStartWidth = (width * 30) / 100;
            int swipeEndWidth = (width * 80) / 100;

            int swipeStartHeight = height / 2;
            int swipeEndHeight = height / 2;

            //appiumDriver.swipe(swipeStartWidth, swipeStartHeight, swipeEndWidth, swipeEndHeight, 1000);

            new TouchAction(driver)
                    .press(PointOption.point(swipeStartWidth, swipeStartHeight))
                    .waitAction(WaitOptions.waitOptions(ofMillis(1000)))
                    .moveTo(PointOption.point(swipeEndWidth, swipeEndHeight))
                    .release()
                    .perform();

        }
    }


    @Step({"<key> li elementin değeri <text> e içerdiğini kontrol et",
            "Find element by <key> and text contains <text>"})
    public void containsTextByKey(String key, String text) {
       /* By by = selector.getElementInfoToBy(key);
        Assert.assertTrue(appiumFluentWait.until(new ExpectedCondition<Boolean>() {
            private String currentValue = null;

            @Override
            public Boolean apply(WebDriver driver) {
                try {
                    currentValue = driver.findElement(by).getText();
                    return currentValue.contains(text);
                } catch (Exception e) {
                    return false;
                }
            }

            @Override
            public String toString() {
                return String.format("text contains be \"%s\". Current text: \"%s\"", text, currentValue);
            }
        }));*/
    }


    @Step({"<key> li elementin değeri <text> e eşitliğini kontrol et",
            "Find element by <key> and text equals <text>"})
    public void equalsTextByKey(String key, String text) {
        //Assert.assertTrue(appiumFluentWait.until(
        //      ExpectedConditions.textToBe(selector.getElementInfoToBy(key), text)));
    }

    @Step({"<seconds> saniye bekle ", "Wait <second> seconds"})
    public void waitBySecond(int seconds) throws InterruptedException {
        Thread.sleep(seconds * 1000);
    }

    @Step("geri butonuna bas")
    public void clickBybackButton() {

        ((AndroidDriver) driver).pressKeyCode(AndroidKeyCode.BACK);



    }


    @Step("OTP Sifre Bilgisi Girilir")
    public void enterOTPValue() {
        ((AndroidDriver<MobileElement>) driver).pressKey(new KeyEvent(AndroidKey.NUMPAD_1));
        ((AndroidDriver<MobileElement>) driver).pressKey(new KeyEvent(AndroidKey.NUMPAD_2));
        ((AndroidDriver<MobileElement>) driver).pressKey(new KeyEvent(AndroidKey.NUMPAD_3));
        ((AndroidDriver<MobileElement>) driver).pressKey(new KeyEvent(AndroidKey.NUMPAD_4));
        ((AndroidDriver<MobileElement>) driver).pressKey(new KeyEvent(AndroidKey.NUMPAD_5));
        ((AndroidDriver<MobileElement>) driver).pressKey(new KeyEvent(AndroidKey.NUMPAD_6));
    }


    public static void sendKeys(String elementValue, String text) {
        wait.until(ExpectedConditions.visibilityOfElementLocated
                (ReadFiles.readLocator(elementValue))).sendKeys(text);

    }

    public static void clickElement(String elementValue) {

        wait.until(ExpectedConditions.visibilityOfElementLocated
                (ReadFiles.readLocator(elementValue))).click();
    }

    public static void clearElement(String elementValue) {

        wait.until(ExpectedConditions.visibilityOfElementLocated
                (ReadFiles.readLocator(elementValue))).clear();
    }
}

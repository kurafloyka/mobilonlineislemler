import com.thoughtworks.gauge.AfterScenario;
import com.thoughtworks.gauge.BeforeScenario;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;

public class HookImplementation  {


    public Logger log = Logger.getLogger(HookImplementation.class);
    public static AppiumDriver<MobileElement> driver;
    public static WebDriverWait wait;
    public static URL url;
    public static DesiredCapabilities capabilities;
    public static Boolean localAndroid = true;


    @BeforeScenario
    public void setup() throws MalformedURLException {


        if (localAndroid) {


            url = new URL("http://0.0.0.0:4723/wd/hub");
            capabilities = new DesiredCapabilities();
            capabilities.setCapability("noReset", "true");
            capabilities.setCapability("fullReset", "false");
            capabilities.setCapability("deviceName", "Android Emulator");
            capabilities.setCapability("platformVersion", "11.0");
            capabilities.setCapability("platformName", "Android");
            //capabilities.setCapability("appPackage", "com.android.chrome");
            //capabilities.setCapability("appActivity", "com.google.android.apps.chrome.Main");
            capabilities.setCapability(MobileCapabilityType.APP, "/Users/mac/Downloads/OnlineIslem-prod.apk");
///Users/mac/Downloads/android-ui-master/example.apk


            driver = new AndroidDriver(url, capabilities);
            wait = new WebDriverWait(driver, 10);


        } else {

            url = new URL("http://0.0.0.0:4723/wd/hub");
            capabilities = new DesiredCapabilities();
            capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
            capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "iOS");
            capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "14.2");
            capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone 11");
            capabilities.setCapability(MobileCapabilityType.APP, "//Users/mac/Library/Developer/Xcode/DerivedData/UIKitCatalog-dtgedxpidruoiwabarsvifjxcccw/Build/Products/Debug-iphonesimulator/UIKitCatalog.app");

            driver = new IOSDriver(url, capabilities);
            wait = new WebDriverWait(driver, 10);

        }


    }

    @Test
    public void androidAppLogin() throws MalformedURLException {


        wait.until(ExpectedConditions.visibilityOfElementLocated
                (By.id("et_msisdn"))).sendKeys("5012223471");
        log.info("GSM NO girildi...");

        wait.until(ExpectedConditions.visibilityOfElementLocated
                (By.id("et_password"))).sendKeys("123456");
        log.info("Sifre girildi...");

        wait.until(ExpectedConditions.visibilityOfElementLocated
                (By.id("btn_login"))).click();

        log.info("Giris butonuna basildi...");


    }


    @Test
    public void iosAppLogin() throws MalformedURLException {


        log.info("IOS cihazini ayaga kaldirdik.....");


    }


    @AfterScenario
    public void close() {

        driver.quit();
    }
}
